const nodeStatic = require('node-static')
const fileServer = new nodeStatic.Server(`${__dirname}/static`)
const fileServerMedia = new nodeStatic.Server(`${__dirname}/media`)

exports.route = (handlers, route, req, res) => {
  if ((typeof handlers[route]) === 'function') {
    handlers[route](req, res)
  } else if (route.match(/^\/download/)) {
    fileServerMedia.serve(req, res)
  } else {
    fileServer.serve(req, res)
  }
}
