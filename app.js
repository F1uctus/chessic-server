const server = require("./server")
const router = require("./router")
const handlers = require("./handlers")

server.start(router.route, {
  '/api/explore': handlers.explore,
  '/api/create': handlers.create,
  '/api/remove': handlers.remove,
  '/api/download': handlers.download,
  '/api/join': handlers.join,
  '/api/login': handlers.login,
  '/api/logout': handlers.logout,
})