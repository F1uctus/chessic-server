const fs = require('fs')
const {URL} = require("url")
const {Items, Users, Tokens} = require('./model')
const js = JSON.stringify
const Zip = require('adm-zip')

/**
 * Функция авторизации пользователя.
 * successCallback вызывается при успешной авторизации с ID пользователя.
 * errorCallback   вызывается при ошибке авторизации с описанием ошибки в виде строки.
 */
function authorize(token, successCallback, errorCallback) {
  if (!token) {
    errorCallback(401, "Not authorized")
  } else {
    Tokens.findAll({
      where: {
        id: token
      }
    }).then(tokens => {
      if (tokens.length > 0) {
        successCallback(tokens[0].dataValues.userId)
      } else {
        errorCallback(401, "Not authorized")
      }
    }).catch(err => {
      console.error(err)
      errorCallback(503, "Database error [1]")
    })
  }
}

/**
 * Функция для чтения данных из запроса и преобразования их в JSON.
 * 'callback' вызывается с одним параметром: данными в JSON.
 */
function readDataFromRequest(request, callback) {
  let body = []
  request.on('data', chunk => {
    body.push(chunk)
  }).on('end', () => {
    if (body.length === 0) {
      body = '{}'
    } else {
      body = body.join('')
    }
    console.log('body', body)
    callback(JSON.parse(body))
  })
}

function archiveFrom(ids) {
  const filepath = `${__dirname}/files.zip`
  Items.findAll({
    where: {id: ids}
  }).then(items => {
    const zip = new Zip(null)
    for (let item of items) {
      zip.addLocalFile(`${__dirname}/media${item.filepath}`, null, null, "")
      console.log(`${__dirname}/media${item.filepath}`)
    }
    zip.writeZip(filepath, console.error)
  }).catch(console.error)
  return filepath
}

const errorHandler = (code, res, err) => {
  console.error(err)
  res.writeHead(code, {"Content-Type": "application/json"})
  res.write(JSON.stringify({error: err}))
  res.end()
}

exports.explore = (req, res) => {
  authorize(
    req.headers.token,
    userId => {
      Items.findAll({
        where: {
          public: true,
        }
      }).then(data => {
        res.writeHead(200, {"Content-Type": "application/json"})
        res.write(js(data.map(e => {
          return {
            id: e.id,
            title: e.title,
            filepath: e.filepath
          }
        })))
        res.end()
      }).catch(err => errorHandler(503, res, err))
    },
    (code, err) => errorHandler(code, res, err)
  )
}

exports.create = (req, res) => {
  readDataFromRequest(req, data => {
    authorize(
      req.headers.token,
      userId => {
        Items.create({
          title: data.title,
          filepath: ''
        }).then(okData => {
          console.log(`result?`, okData)
          const imageContent = data.image
          const buffer = Buffer.from(imageContent, 'base64')
          const clientFilePath = `/download/${okData.id}-${data.title}`
          const filePath = `${__dirname}/media/${clientFilePath}`

          fs.writeFile(filePath, buffer, 'binary', err => {
            if (!err) {
              okData.filepath = clientFilePath
              okData.save()
              res.writeHead(200, {"Content-Type": "application/json"})
              res.end()
            } else {
              errorHandler(401, res, err)
            }
          })
        }).catch(err => errorHandler(503, res, err))
      },
      (code, err) => errorHandler(code, res, err)
    )
  })
}

exports.remove = (req, res) => {
  authorize(
    req.headers.token,
    userId => {
      for (let id of new URL(req.url).query.replace(/^ids=/, '').split('%2C')) {
        Items.destroy({
          where: {id: id}
        })
      }
      res.writeHead(204, {"Content-Type": "application/json"})
      res.end()
    },
    (code, err) => errorHandler(code, res, err)
  )
}

exports.download = (req, res) => {
  authorize(
    req.headers.token,
    userId => {
      const filePath = archiveFrom(
        new URL(req.url).query.replace(/^ids=/, '').split('%2C')
      )
      const stat = fs.statSync(filePath)
      res.writeHead(200, {
        'Content-Type': 'application/zip',
        'Content-Length': stat.size
      })
      const readStream = fs.createReadStream(filePath)
      readStream.on('open', () => {
        readStream.pipe(res)
      })
      readStream.on('error', err => {
        res.end(err)
      })
    },
    (code, err) => errorHandler(code, res, err)
  )
}

exports.join = (req, res) => {
  readDataFromRequest(req, data => {
    Users.create({
      username: data.username,
      email: data.email,
      password: data.password,
    }).then(ok => {
      res.writeHead(204, {"Content-Type": "application/json"})
      res.end()
    }).catch(err => errorHandler(503, res, "Could not create user."))
  })
}

exports.login = (req, res) => {
  readDataFromRequest(req, data => {
    Users.findAll({
      where: {
        username: data.username,
        password: data.password
      }
    }).then(users => {
      if (users.length > 0) {
        const userId = users[0].dataValues.id
        const login = users[0].dataValues.login
        const tokenTTL = 60;
        const today = Date.now()
        const expire = today + tokenTTL * 60 * 1000
        Tokens.create({
          userId: userId,
          expire: expire
        }).then(data => {
          res.writeHead(201, {"Content-Type": "application/json"})
          res.write(JSON.stringify({login: login, token: data.dataValues.id}))
          res.end()
        }).catch(err => errorHandler(503, res, err))
      } else {
        errorHandler(401, res, "Wrong login or password.")
      }
    }).catch(err => errorHandler(503, res, "Database error [2]"))
  })
}

exports.logout = (req, res) => {
  readDataFromRequest(req, data => {
    authorize(
      req.headers.token,
      userId => {
        const token = req.headers.token
        Tokens.findAll({
          where: {
            id: token
          }
        }).then(data => {
          if (data.length > 0) {
            Tokens.destroy({
              where: {id: token}
            }).then(data => {
              res.writeHead(204, {"Content-Type": "application/json"})
              res.end()
            })
          } else {
            errorHandler(401, res, "Not authorized.")
          }
        }).catch(err => errorHandler(503, res, "Database error [3]"))
      },
      (code, err) => errorHandler(code, res, err)
    )
  })
}