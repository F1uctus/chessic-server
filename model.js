const {Sequelize, Model, DataTypes} = require('sequelize')

const sequelize = new Sequelize(
  'chessic',
  'root',
  'toortik',
  {host: 'localhost', port: 3306, dialect: 'mariadb'}
)


class Items extends Model {
}

Items.init({
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  title: DataTypes.STRING,
  filepath: DataTypes.STRING,
  public: DataTypes.BOOLEAN,
  owner: {
    type: DataTypes.UUID,
    references: {
      model: 'users',
      key: 'id'
    }
  }
}, {
  sequelize,
  modelName: 'items'
})
Items.sync()
exports.Items = Items


class Users extends Model {
}

Users.init({
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  username: DataTypes.STRING,
  email: DataTypes.STRING,
  password: DataTypes.STRING,
}, {
  sequelize,
  modelName: 'users'
})
Users.sync()
Users.hasMany(Items)
exports.Users = Users


class Tokens extends Model {
}

Tokens.init({
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  userId: DataTypes.UUID,
  expire: DataTypes.BIGINT
}, {
  sequelize,
  modelName: 'tokens'
})
Tokens.sync()
exports.Tokens = Tokens
