const http = require("http")
const {URL} = require("url")

exports.start = (route, handle) => {
  const port = 3001
  http.createServer((req, res) => {
    route(handle, new URL(req.url, 'http://' + req.headers.host + '/').pathname, req, res)
  }).listen(port)
  console.log(`Server started on port ${port}`)
}